/**
 * Endpoints
 */

const Google = {
  GET_LOCATION: 'https://maps.googleapis.com/maps/api/geocode/json',
};

const Tourmega = {
  ALL_TOUR: ' https://staging.tourmega.com/api/v2/tours',
};

export const EndPoint = {
  Google,
  Tourmega,
};
