import { AppConfig } from '../../configs';
import { EndPoint } from '../EndPoint';
import { HttpMethod } from '../HttpMethod';
import RestClient from '../RestClient';

const getLocation = async params => {
  const result = await RestClient.request({
    method: HttpMethod.GET,
    url: `${EndPoint.Google.GET_LOCATION}?address=${params.address}&key=${AppConfig.GOOGLE_API_KEY}`,
    data: {},
    useToast: false,
    useToken: false,
  });
  return result;
};

const LocationService = {
  getLocation,
};

export default LocationService;
