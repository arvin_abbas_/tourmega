import { AppConfig } from '../../configs';
import { EndPoint } from '../EndPoint';
import { HttpMethod } from '../HttpMethod';
import RestClient from '../RestClient';

const getAllTours = async params => {
  const result = await RestClient.request({
    method: HttpMethod.GET,
    url: `${EndPoint.Tourmega.ALL_TOUR}?lat=${params?.lat}&lng=${params?.lng}`,
    data: {},
    useToast: false,
    useToken: false,
  });
  return result;
};

const TourMegaService = {
  getAllTours,
};

export default TourMegaService;
