export { default as RestClient } from './RestClient';
export { default as HttpMethod } from './HttpMethod';
export { default as LocationService } from './Services/LocationService';
export { default as TourMegaService } from './Services/TourMegaService';
