import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  getLocation: ['params', 'meta'],
  getLocationLoading: null,
  getLocationSuccess: ['resultGetLocation'],
  getLocationFailure: ['errorMessage'],
});

export const LocationTypes = Types;
export default Creators;
