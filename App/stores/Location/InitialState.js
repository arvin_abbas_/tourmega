/**
 * The initial values for the redux helper states.
 */
export const INITIAL_STATE = {
  loadingGetLocation: false,
  resultGetLocation: [],
  failurMessageGetLocation: null,
};
