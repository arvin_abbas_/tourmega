import { put, call, all, takeLatest } from 'redux-saga/effects';
import { LocationService } from '../../api';
import LocationActions, { LocationTypes } from './Actions';

function* getLocation(action) {
  yield put(LocationActions.getLocationLoading());
  const result = yield call(LocationService.getLocation, action.params);
  if (result.status === 'OK') {
    yield put(LocationActions.getLocationSuccess(result?.results));
  } else {
    yield put(LocationActions.getLocationFailure(result?.message));
  }
}

export default function* () {
  yield all([takeLatest(LocationTypes.GET_LOCATION, getLocation)]);
}
