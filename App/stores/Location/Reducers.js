import { createReducer } from 'reduxsauce';
import { INITIAL_STATE } from './InitialState';
import { LocationTypes } from './Actions';

export const getLocationLoading = state => ({
  ...state,
  loadingGetLocation: true,
  failurMessageGetLocation: null,
});

export const getLocationSuccess = (state, { resultGetLocation }) => ({
  ...state,
  loadingGetLocation: false,
  resultGetLocation,
  failurMessageGetLocation: null,
});

export const getLocationFailure = (state, { errorMessage }) => ({
  ...state,
  loadingGetLocation: false,
  resultGetLocation: [],
  failurMessageGetLocation: errorMessage,
});

export const reducer = createReducer(INITIAL_STATE, {
  [LocationTypes.GET_LOCATION_LOADING]: getLocationLoading,
  [LocationTypes.GET_LOCATION_SUCCESS]: getLocationSuccess,
  [LocationTypes.GET_LOCATION_FAILURE]: getLocationFailure,
});
