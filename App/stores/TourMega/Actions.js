import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  getTours: ['params', 'meta'],
  getToursLoading: null,
  getToursSuccess: ['resultGetTours'],
  getToursFailure: ['errorMessage'],
});

export const TourMegaTypes = Types;
export default Creators;
