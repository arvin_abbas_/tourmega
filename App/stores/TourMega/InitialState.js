/**
 * The initial values for the redux helper states.
 */
export const INITIAL_STATE = {
  loadingGetTours: false,
  resultGetTours: [],
  failurMessageGetTours: null,
};
