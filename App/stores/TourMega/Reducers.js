import { createReducer } from 'reduxsauce';
import { INITIAL_STATE } from './InitialState';
import { TourMegaTypes } from './Actions';

export const getToursLoading = state => ({
  ...state,
  loadingGetTours: true,
  failurMessageGetTours: null,
});

export const getToursSuccess = (state, { resultGetTours }) => ({
  ...state,
  loadingGetTours: false,
  resultGetTours,
  failurMessageGetTours: null,
});

export const getToursFailure = (state, { errorMessage }) => ({
  ...state,
  loadingGetTours: false,
  resultGetTours: [],
  failurMessageGetTours: errorMessage,
});

export const reducer = createReducer(INITIAL_STATE, {
  [TourMegaTypes.GET_TOURS_LOADING]: getToursLoading,
  [TourMegaTypes.GET_TOURS_SUCCESS]: getToursSuccess,
  [TourMegaTypes.GET_TOURS_FAILURE]: getToursFailure,
});
