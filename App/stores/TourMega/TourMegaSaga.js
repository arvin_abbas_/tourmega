import { put, call, all, takeLatest } from 'redux-saga/effects';
import { TourMegaService } from '../../api';
import TourMegaActions, { TourMegaTypes } from './Actions';

function* getTours(action) {
  yield put(TourMegaActions.getToursLoading());
  const result = yield call(TourMegaService.getAllTours, action.params);
  if (result.success) {
    yield put(TourMegaActions.getToursSuccess(result?.data));
  } else {
    yield put(TourMegaActions.getToursFailure(result?.message));
  }
}

export default function* () {
  yield all([takeLatest(TourMegaTypes.GET_TOURS, getTours)]);
}
