import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from '@react-native-community/async-storage';
import { reducer as NetworkReducer } from 'react-native-offline';
import rootSaga from './Saga';
import configureStore from './CreateStore';
import { reducer as LocationReducer } from './Location/Reducers';
import { reducer as TourMegaReducer } from './TourMega/Reducers';

// Add a nested state of reducer for rehydrated
const locationPersistConfig = {
  key: 'location',
  storage,
  blacklist: ['loadingGetLocation', 'resultGetLocation'],
};

// Add a nested state of reducer for rehydrated
const tourmegaPersistConfig = {
  key: 'tourmega',
  storage,
  blacklist: ['loadingGetTours', 'resultGetTours'],
};

// Add a nested state of reducer for rehydrated
const networkPersistConfig = {
  key: 'network',
  storage,
  blacklist: ['isConnected'],
};

export default () => {
  const rootReducer = combineReducers({
    network: persistReducer(networkPersistConfig, NetworkReducer),
    location: persistReducer(locationPersistConfig, LocationReducer),
    tourmega: persistReducer(tourmegaPersistConfig, TourMegaReducer),
  });

  return configureStore(rootReducer, rootSaga);
};
